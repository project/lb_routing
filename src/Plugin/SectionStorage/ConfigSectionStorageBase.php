<?php

namespace Drupal\lb_routing\Plugin\SectionStorage;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\layout_builder\Plugin\SectionStorage\SectionStorageBase;
use Drupal\layout_builder\Plugin\SectionStorage\SectionStorageLocalTaskProviderInterface;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionListTrait;
use Drupal\layout_builder\SectionStorage\SectionStorageDefinition;
use Drupal\lb_routing\LayoutBuilderRoutingServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides section storage base class.
 */
abstract class ConfigSectionStorageBase extends SectionStorageBase implements SectionStorageLocalTaskProviderInterface, ContainerFactoryPluginInterface {

  use SectionListTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Layout Builder routing service.
   *
   * @var \Drupal\lb_routing\LayoutBuilderRoutingServiceInterface
   */
  protected $routingService;

  /**
   * An array of sections.
   *
   * @var \Drupal\layout_builder\Section[]|null
   */
  protected $sections;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, LayoutBuilderRoutingServiceInterface $lb_routing_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->routingService = $lb_routing_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('lb_routing.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->getStorageId();
  }

  /**
   * Returns the name to be used to store in the config system.
   */
  protected function getConfigName() {
    return 'lb_routing.' . $this->getStorageType() . '.' . $this->getStorageId();
  }

  /**
   * {@inheritdoc}
   */
  public function getSections() {
    if (is_null($this->sections)) {
      $sections = $this->configFactory->get($this->getConfigName())->get('sections') ?: [];
      $this->setSections(array_map([Section::class, 'fromArray'], $sections));
    }
    return $this->sections;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSectionList() {
    return $this->getContextValue('display');
  }

  /**
   * {@inheritdoc}
   */
  protected function setSections(array $sections) {
    $this->sections = array_values($sections);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    $sections = array_map(function (Section $section) {
      return $section->toArray();
    }, $this->getSections());

    $config = $this->configFactory->getEditable($this->getConfigName());
    $return = $config->get('sections') ? SAVED_UPDATED : SAVED_NEW;
    $config->set('sections', $sections)->save();

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function buildLocalTasks($base_plugin_definition) {
    $type = $this->getStorageType();
    $local_tasks = [];

    foreach ($this->routingService->getEnabledRoutes() as $route_name) {
      $local_tasks["lb_builder.$type.$route_name"] = $base_plugin_definition + [
          'route_name' => $route_name,
          'title' => $this->t('Content'),
          'base_route' => "layout_builder.$type.view",
          'route_parameters' => ['id' => $route_name],
          'weight' => 0,
        ];
    }

    $local_tasks["layout_builder.$type.view"] = $base_plugin_definition + [
        'route_name' => "layout_builder.$type.view",
        'title' => $this->t('Layout'),
        'base_route' => "layout_builder.$type.view",
        'weight' => 10,
      ];

    $local_tasks["layout_builder.$type.view__child"] = $base_plugin_definition + [
        'route_name' => "layout_builder.$type.view",
        'title' => $this->t('Layout'),
        'parent_id' => "layout_builder_ui:layout_builder.$type.view",
      ];
    $local_tasks["layout_builder.$type.discard_changes"] = $base_plugin_definition + [
        'route_name' => "layout_builder.$type.discard_changes",
        'title' => $this->t('Discard changes'),
        'parent_id' => "layout_builder_ui:layout_builder.$type.view",
        'weight' => 5,
      ];

    return $local_tasks;
  }

  /**
   * {@inheritdoc}
   */
  public function getLayoutBuilderUrl($rel = 'view') {
    return Url::fromRoute("layout_builder.{$this->getStorageType()}.$rel", ['id' => $this->getStorageId()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirectUrl() {
    return $this->getLayoutBuilderUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    //@TODO Access needs to be controlled.
    $result = AccessResult::allowed();
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(RefinableCacheableDependencyInterface $cacheability) {
    $cacheability->addCacheableDependency($this);
    return TRUE;
  }

  /**
   * Builds the layout routes for the given values.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   The route collection.
   * @param \Drupal\layout_builder\SectionStorage\SectionStorageDefinition $definition
   *   The definition of the section storage.
   * @param string $path
   *   The path patten for the routes.
   * @param array $defaults
   *   (optional) An array of default parameter values.
   * @param array $requirements
   *   (optional) An array of requirements for parameters.
   * @param array $options
   *   (optional) An array of options.
   * @param string $route_name_prefix
   *   (optional) The prefix to use for the route name.
   * @param string $entity_type_id
   *   (optional) The entity type ID, if available.
   */
  protected function buildLayoutRoutes(RouteCollection $collection, SectionStorageDefinition $definition, $path, array $defaults = [], array $requirements = [], array $options = [], $route_name_prefix = '', $entity_type_id = '') {
    $type = $definition->id();
    $defaults['section_storage_type'] = $type;
    // Provide an empty value to allow the section storage to be upcast.
    $defaults['section_storage'] = '';
    // Trigger the layout builder access check.
    $requirements['_layout_builder_access'] = 'view';
    // Trigger the layout builder RouteEnhancer.
    $options['_layout_builder'] = TRUE;
    // Trigger the layout builder param converter.
    $parameters['section_storage']['layout_builder_tempstore'] = TRUE;
    // Merge the passed in options in after Layout Builder's parameters.
    $options = NestedArray::mergeDeep(['parameters' => $parameters], $options);

    if ($route_name_prefix) {
      $route_name_prefix = "layout_builder.$type.$route_name_prefix";
    }
    else {
      $route_name_prefix = "layout_builder.$type";
    }

    $main_defaults = $defaults;
    $main_options = $options;
    $main_defaults['_form'] = '\Drupal\lb_routing\Form\LayoutBuilderForm';

    $main_defaults['_title_callback'] = '\Drupal\layout_builder\Controller\LayoutBuilderController::title';
    $route = (new Route($path))
      ->setDefaults($main_defaults)
      ->setRequirements($requirements)
      ->setOptions($main_options);
    $collection->add("$route_name_prefix.view", $route);

    $discard_changes_defaults = $defaults;
    $discard_changes_defaults['_form'] = '\Drupal\layout_builder\Form\DiscardLayoutChangesForm';
    $route = (new Route("$path/discard-changes"))
      ->setDefaults($discard_changes_defaults)
      ->setRequirements($requirements)
      ->setOptions($options);
    $collection->add("$route_name_prefix.discard_changes", $route);
  }

}
