<?php

namespace Drupal\lb_routing\Plugin\SectionStorage;

use Drupal\Component\Plugin\Context\ContextInterface as ComponentContextInterface;
use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\ContextInterface;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides simple section storage for lb_routing in config.
 *
 * @SectionStorage(
 *   id = "lb_routing_config_simple",
 *   weight = 99,
 *   context_definitions = {
 *     "config_id" = @ContextDefinition("string"),
 *   }
 * )
 */
class SimpleConfigSectionStorage extends ConfigSectionStorageBase {

  /**
   * {@inheritdoc}
   */
  public function getStorageId() {
    return $this->getContextValue('config_id');
  }

  /**
   * {@inheritdoc}
   */
  public function deriveContextsFromRoute($value, $definition, $name, array $defaults) {
    $contexts = [];
    $value = $value ?: $defaults['id'];
    $contexts['config_id'] = new Context(new ContextDefinition('string'), $value);
    return $contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function setContext($name, ComponentContextInterface $context) {
    // Check that the context passed is an instance of our extended interface.
    if (!$context instanceof ContextInterface) {
      throw new ContextException("Passed $name context must be an instance of \\Drupal\\Core\\Plugin\\Context\\ContextInterface");
    }
    $this->context[$name] = $context;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRoutes(RouteCollection $collection) {
    $this->buildLayoutRoutes($collection, $this->getPluginDefinition(), 'lb_routing/lb_routing_config_simple/{id}');
  }

}
