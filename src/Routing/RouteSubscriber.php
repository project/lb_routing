<?php

namespace Drupal\lb_routing\Routing;

use Drupal\Core\Routing\RouteObjectInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\Core\State\StateInterface;
use Drupal\lb_routing\Controller\LayoutBuilderRoutingController;
use Drupal\lb_routing\LayoutBuilderRoutingServiceInterface;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The Layout Builder routing service.
   *
   * @var \Drupal\lb_routing\LayoutBuilderRoutingServiceInterface
   */
  protected $routingService;

  /**
   * The state key value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new RouteSubscriber object.
   *
   * @param \Drupal\lb_routing\LayoutBuilderRoutingServiceInterface $lb_routing_service
   *   The Layout Builder routing service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key value store.
   */
  public function __construct(LayoutBuilderRoutingServiceInterface $lb_routing_service, StateInterface $state) {
    $this->routingService = $lb_routing_service;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[RoutingEvents::FINISHED] = ['routeRebuildFinished', -100];
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -200];
    return $events;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($this->routingService->getEnabledRoutes() as $route_name) {
      if ($route = $collection->get($route_name)) {
        $route->setDefaults([
          RouteObjectInterface::CONTROLLER_NAME => LayoutBuilderRoutingController::class . '::render',
          'id' => $route_name,
        ]);
      }
    }
  }

  /**
   * Overrides views routes.
   *
   * Callback for the RoutingEvents::FINISHED event.
   *
   * @see \Drupal\views\EventSubscriber::routeRebuildFinished()
   */
  public function routeRebuildFinished() {
    $views_route_names = $this->state->get('views.view_route_names');
    foreach ($this->routingService->getEnabledRoutes() as $route_name) {
      if (str_starts_with($route_name, 'view.')) {
        $view_route_name = str_replace('view.', '', $route_name);
        if (isset($views_route_names[$view_route_name])) {
          unset($views_route_names[$view_route_name]);
        }
      }
    }
    $this->state->set('views.view_route_names', $views_route_names);
  }

}
