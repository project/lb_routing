<?php

namespace Drupal\lb_routing\Form;

use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\Form\PreviewToggleTrait;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Drupal\layout_builder\SectionComponent;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form containing the Layout Builder UI.
 */
class LayoutBuilderForm extends FormBase {

  use PreviewToggleTrait;

  /**
   * Layout tempstore repository.
   *
   * @var \Drupal\layout_builder\LayoutTempstoreRepositoryInterface
   */
  protected $layoutTempstoreRepository;

  /**
   * The section storage.
   *
   * @var \Drupal\layout_builder\SectionStorageInterface
   */
  protected $sectionStorage;

  /**
   * Constructs a new LayoutBuilderForm.
   *
   * @param \Drupal\layout_builder\LayoutTempstoreRepositoryInterface $layout_tempstore_repository
   *   The layout tempstore repository.
   */
  public function __construct(LayoutTempstoreRepositoryInterface $layout_tempstore_repository) {
    $this->layoutTempstoreRepository = $layout_tempstore_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('layout_builder.tempstore_repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lb_routing_default_layout';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SectionStorageInterface $section_storage = NULL) {
    // @TODO Unsaved changes might display incorrectly until
    // https://www.drupal.org/project/drupal/issues/3207875 is fixed.
    $form['#attributes']['class'][] = 'layout-builder-form';

    $form['layout_builder'] = [
      '#type' => 'layout_builder',
      '#section_storage' => $section_storage,
      '#process' => [[static::class, 'layoutBuilderElementGetKeys']],
    ];
    $this->sectionStorage = $section_storage;

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save layout'),
      '#button_type' => 'primary',
    ];
    $form['actions']['preview_toggle'] = $this->buildContentPreviewToggle();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // For inline blocks, we need to save them in a way they can be loaded
    // by revision.
    $sections = $this->sectionStorage->getSections();
    foreach ($this->getInlineBlockComponents($sections) as $component) {
      $this->saveInlineBlockComponent($component);
    }

    $this->sectionStorage->save();
    $this->layoutTempstoreRepository->delete($this->sectionStorage);
    $this->messenger()->addMessage($this->t('The layout has been saved.'));
    $form_state->setRedirectUrl($this->sectionStorage->getRedirectUrl());
  }

  /**
   * Form element #process callback.
   *
   * Save the layout builder element array parents as a property on the top form
   * element so that they can be used to access the element within the whole
   * render array later.
   *
   * @see \Drupal\layout_builder\Controller\LayoutBuilderHtmlEntityFormController
   */
  public static function layoutBuilderElementGetKeys(array $element, FormStateInterface $form_state, &$form) {
    $form['#layout_builder_element_keys'] = $element['#array_parents'];
    return $element;
  }

  /**
   * Gets components that have Inline Block plugins.
   *
   * @see \Drupal\layout_builder\LayoutEntityHelperTrait::getInlineBlockComponents()
   *
   * @param \Drupal\layout_builder\Section[] $sections
   *   The layout sections.
   *
   * @return \Drupal\layout_builder\SectionComponent[]
   *   The components that contain Inline Block plugins.
   */
  protected function getInlineBlockComponents(array $sections) {
    $inline_block_components = [];
    foreach ($sections as $section) {
      foreach ($section->getComponents() as $component) {
        $plugin = $component->getPlugin();
        if ($plugin instanceof DerivativeInspectionInterface && $plugin->getBaseId() === 'inline_block') {
          $inline_block_components[] = $component;
        }
      }
    }
    return $inline_block_components;
  }

  /**
   * Saves an inline block component.
   *
   * @see \Drupal\layout_builder\InlineBlockEntityOperations::saveInlineBlockComponent()
   *
   * @param \Drupal\layout_builder\SectionComponent $component
   *   The section component with an inline block.
   */
  protected function saveInlineBlockComponent(SectionComponent $component) {
    /** @var \Drupal\layout_builder\Plugin\Block\InlineBlock $plugin */
    $plugin = $component->getPlugin();
    $plugin->saveBlockContent(TRUE);
    $post_save_configuration = $plugin->getConfiguration();
    $component->setConfiguration($post_save_configuration);
  }

}
