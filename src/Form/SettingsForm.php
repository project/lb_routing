<?php

namespace Drupal\lb_routing\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\Section;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure LB Routing module settings.
 */
class SettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'lb_routing.settings';

  /**
   * The Layout Builder routing service.
   *
   * @var \Drupal\lb_routing\LayoutBuilderRoutingServiceInterface
   */
  protected $routingService;

  /**
   * The router builder service.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routerBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->routingService = $container->get('lb_routing.service');
    $instance->routerBuilder = $container->get('router.builder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lb_routing_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['enabled_routes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Enabled routes'),
      '#description' => $this->t('Specify route names to enable LB for. Enter one route per line.'),
      '#default_value' => $config->get('enabled_routes'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $route_names = array_filter(preg_split("/(\r\n|\n|\r)/", $form_state->getValue('enabled_routes')));
    // Get the config names from the routes.
    $config_names = [];
    foreach ($route_names as $route_name) {
      $config_names[] = 'lb_routing.lb_routing_config_simple.' . $route_name;
    }
    foreach ($config_names as $config_name) {
      // Only create the config when it doesn't exist to avoid overriding
      // already configured pages.
      if ($this->configFactory->get($config_name)->isNew()) {
        $this->configFactory->getEditable($config_name)
          ->set('sections', [(new Section('layout_twocol'))->toArray()])
          ->save();
      }
    }

    // Delete configs that are not present anymore.
    $previous_enabled_routes = $this->routingService->getEnabledRoutes();
    foreach (array_diff($previous_enabled_routes, $route_names) as $config_to_delete) {
      $config_name = $this->routingService->getConfigName($config_to_delete);
      $this->configFactory->getEditable($config_name)->delete();
    }

    // Save the config value.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('enabled_routes', $form_state->getValue('enabled_routes'))
      ->save();
    parent::submitForm($form, $form_state);

    // Ensure the routes are rebuilt when overriding them.
    $this->routerBuilder->rebuild();
  }

}
