<?php

namespace Drupal\lb_routing\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\layout_builder\SectionStorage\SectionStorageManager;
use Drupal\lb_routing\LayoutBuilderRoutingServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Layout Builder routing controller.
 *
 * @ingroup lb_routing
 */
class LayoutBuilderRoutingController extends ControllerBase {

  /**
   * The Layout Builder routing service.
   *
   * @var \Drupal\lb_routing\LayoutBuilderRoutingServiceInterface
   */
  protected $routingService;

  /**
   * The context manager service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * The Section Storage Manager.
   *
   * @var \Drupal\layout_builder\SectionStorage\SectionStorageManager
   */
  protected $sectionStorageManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new LayoutBuilderRoutingController object.
   *
   * @param \Drupal\lb_routing\LayoutBuilderRoutingServiceInterface $lb_routing_service
   *   The Layout Builder routing service.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   The lazy context repository service.
   * @param \Drupal\layout_builder\SectionStorage\SectionStorageManager $section_storage_manager
   *   The section storage manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(LayoutBuilderRoutingServiceInterface $lb_routing_service, ContextRepositoryInterface $context_repository, SectionStorageManager $section_storage_manager, RouteMatchInterface $route_match) {
    $this->routingService = $lb_routing_service;
    $this->contextRepository = $context_repository;
    $this->sectionStorageManager = $section_storage_manager;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('lb_routing.service'),
      $container->get('context.repository'),
      $container->get('plugin.manager.layout_builder.section_storage'),
      $container->get('current_route_match')
    );
  }

  /**
   * Builds a generic render page.
   *
   * @return array
   *   Render array of the sections.
   */
  public function render(): array {
    try {
      $build = [];
      $available_context_ids = array_keys(
        $this->contextRepository->getAvailableContexts()
      );
      $contexts = $this->contextRepository->getRuntimeContexts($available_context_ids);
      $route_name = $this->routeMatch->getRouteName();

      $contexts['config_id'] = new Context(
        new ContextDefinition('string'),
        $this->routingService->getConfigName($route_name, FALSE),
      );

      $cacheability = new CacheableMetadata();
      $storage = $this->sectionStorageManager->findByContext($contexts, $cacheability);
      if ($storage) {
        foreach ($storage->getSections() as $delta => $section) {
          $build[$delta] = $section->toRenderArray($contexts);
        }
      }

      $cacheability->applyTo($build);
    }
    catch (\Exception $e) {
      watchdog_exception('lb_routing', $e);
      return [];
    }

    return $build;
  }

}
