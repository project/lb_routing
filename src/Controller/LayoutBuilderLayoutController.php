<?php

namespace Drupal\lb_routing\Controller;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Controller\FormController;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;

/**
 * Overrides the form controller service for lb_routing operations.
 *
 * @see \Drupal\layout_builder\Controller\LayoutBuilderHtmlEntityFormController
 */
class LayoutBuilderLayoutController extends FormController {

  use DependencySerializationTrait;

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * Constructs a new LayoutBuilderLayoutController object.
   *
   * @param \Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface $argument_resolver
   *   The argument resolver.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver.
   */
  public function __construct(ArgumentResolverInterface $argument_resolver, FormBuilderInterface $form_builder, ClassResolverInterface $class_resolver) {
    parent::__construct($argument_resolver, $form_builder);
    $this->classResolver = $class_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function getContentResult(Request $request, RouteMatchInterface $route_match) {
    $form = parent::getContentResult($request, $route_match);

    // If the form render element has a #layout_builder_element_keys property,
    // first set the form element as a child of the root render array. Use the
    // keys to get the layout builder element from the form render array and
    // copy it to a separate child element of the root element to prevent any
    // forms within the layout builder element from being nested.
    if (isset($form['#layout_builder_element_keys'])) {
      $build['form'] = &$form;
      $layout_builder_element = &NestedArray::getValue($form, $form['#layout_builder_element_keys']);
      $build['layout_builder'] = $layout_builder_element;
      // Remove the layout builder element within the form.
      $layout_builder_element = [];
      return $build;
    }

    // If no #layout_builder_element_keys property, return form as is.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFormArgument(RouteMatchInterface $route_match) {
    return $route_match->getRouteObject()->getDefault('_form');
  }

  /**
   * {@inheritdoc}
   */
  protected function getFormObject(RouteMatchInterface $route_match, $form_arg) {
    return $this->classResolver->getInstanceFromDefinition($form_arg);
  }

}
