<?php

namespace Drupal\lb_routing;

/**
 * Interface for LayoutBuilderRoutingService.
 */
interface LayoutBuilderRoutingServiceInterface {

  /**
   * Gets the enabled routes from config.
   *
   * @param bool $validate
   *    TRUE to only include existing routes.
   *
   * @return array
   *    Array of route names.
   */
  public function getEnabledRoutes(bool $validate = FALSE):array;

  /**
   * Gets the fully named configuration name from a route.
   *
   * @param string $route_name
   *    The route name.
   * @param bool $full
   *    Whether the full config name needs to be returned.
   *
   * @return string
   *    Configuration name.
   */
  public function getConfigName(string $route_name, bool $full = TRUE): string;

}
