<?php

namespace Drupal\lb_routing\EventSubscriber;

use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Drupal\layout_builder\LayoutBuilderEvents;
use Drupal\layout_builder\Plugin\Block\InlineBlock;
use Drupal\lb_routing\Access\InlineBlockAccess;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Adds access dependency to inline blocks to mimic entity based layout.
 *
 * @see \Drupal\layout_builder\EventSubscriber\BlockComponentRenderArray()
 */
class AccessDependencyInlineBlock implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY] = ['onBuildRender', 200];
    return $events;
  }

  /**
   * Builds render arrays for block plugins and sets it on the event.
   *
   * @param \Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent $event
   *   The section component render event.
   */
  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event) {
    $block = $event->getPlugin();
    // We want to avoid messing with core's layout builder and use it only for
    // lb_builder plugins, as we don't have a fully reliable way to identify
    // those plugins, we are extra cautious on what we do.
    // @TODO Add a context with the storage id.
    if (!$block instanceof InlineBlock) {
      return;
    }
    $contexts = $event->getContexts();
    if (isset($contexts['layout_builder.entity'])) {
      return;
    }
    if (!isset($contexts['config_id'])) {
      return;
    }

    $block->setAccessDependency(new InlineBlockAccess());
  }

}
