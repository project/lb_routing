<?php

namespace Drupal\lb_routing\Access;

use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Layout builder routing access for inline blocks.
 */
class InlineBlockAccess implements AccessibleInterface {

  /**
   * {@inheritdoc}
   */
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    // This allows access freely as the route should control the route instead.
    // If the user accessing the route can view it, we consider that the inline
    // blocks should be visible too.
    // If you don't agree, open an issue in
    // https://www.drupal.org/project/lb_routing :)
    if ($operation === 'view') {
      return $return_as_object ? AccessResult::allowed() : TRUE;
    }
    return $return_as_object ? AccessResult::forbidden() : FALSE;
  }

}
