<?php

namespace Drupal\lb_routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Routing\RouteProviderInterface;

/**
 * Layout Builder common service.
 */
class LayoutBuilderRoutingService implements LayoutBuilderRoutingServiceInterface {

  use DependencySerializationTrait;

  /**
   * The route provider to load routes by name.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a LayoutBuilderRoutingService object.
   *
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider to load routes by name.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(RouteProviderInterface $route_provider, ConfigFactoryInterface $config_factory) {
    $this->routeProvider = $route_provider;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getEnabledRoutes(bool $validate = FALSE): array {
    $route_names = array_filter(preg_split("/(\r\n|\n|\r)/", $this->configFactory->get('lb_routing.settings')->get('enabled_routes')));
    if ($validate) {
      $route_names = array_filter($route_names, function ($route_name) {
        try {
          $route = $this->routeProvider->getRouteByName($route_name);
          return !empty($route);
        }
        catch (\Exception $e) {
          return FALSE;
        }
      });
    }
    return $route_names;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigName(string $route_name, bool $full = TRUE): string {
    if ($full) {
      return 'lb_routing.lb_routing_config_section_storage.' . $route_name;
    }
    else {
      return $route_name;
    }
  }

}
